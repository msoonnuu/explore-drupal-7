<?php


function geq_welcome_test(){

 global $user;    
 
 if($user->uid<=0){
    $url_maths  = url('fblogin/facebook_login',array('query'=>array('destination'=>'geq/start_test/maths')));
    $url_english  = url('fblogin/facebook_login',array('query'=>array('destination'=>'geq/start_test/english')));
 }   
 else{
     
     $url_maths  = url('geq/start_test/maths',array());
     $url_english  = url('geq/start_test/english',array());
 }   
    
    
    
 $render_arr = array(
                t('maths')  => array(
                  '#markup' => t('<a href="!url">Start Maths Test</a>',array('!url'=>$url_maths)),
                  '#type'=>'markup',
                  '#prefix'=>t('<div class="!class">',array('!class'=>'mani')),
                  '#suffix'=>t('</div>')  
                ),
                t('english')  => array(
                  '#markup' => t('<a href="!url">Start English Test</a>',array('!url'=>$url_english)),
                  '#prefix'=>t('<div class="!class">',array('!class'=>'mani')),
                  '#suffix'=>t('</div>')  
                ) ,
                '#theme_wrappers' => array('geq_add_div')
  );    
  return $render_arr;
}

?>
