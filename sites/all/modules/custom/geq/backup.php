<?PHP

/**
 * Provides a list of existing entities and the ability to add more. Tabs
 * provide field and display management.
 */
function geq_anw_admin_page() {
  $content = array();
  $content[] = array(
    '#type' => 'item',
    '#markup' => t('Administration page for Entity Example Basic Entities.')
  );

  $content[] = array(
    '#type' => 'item',
    '#markup' => l(t('Add an geq_anw entity'), 'examples/geq/basic/add'),
  );

  $content['table'] = geq_anw_list_entities();

  return $content;
}

/**
 * Returns a render array with all geq_anw entities.
 *
 * In this basic example we know that there won't be many entities,
 * so we'll just load them all for display. See pager_example.module
 * to implement a pager. Most implementations would probably do this
 * with the contrib Entity API module, or a view using views module,
 * but we avoid using non-core features in the Examples project.
 *
 * @see pager_example.module
 */
function geq_anw_list_entities() {
  $content = array();
  // Load all of our entities.
  $entities = geq_anw_load_multiple();
  if (!empty($entities)) {
    foreach ( $entities as $entity ) {
      // Create tabular rows for our entities.
      $rows[] = array(
        'data' => array(
          'id' => $entity->basic_id,
          'item_description' => l($entity->item_description, 'examples/geq/basic/' . $entity->basic_id),
          'bundle' => $entity->bundle_type,
        ),
      );
    }
    // Put our entities into a themed table. See theme_table() for details.
    $content['entity_table'] = array(
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => array(t('ID'), t('Item Description'), t('Bundle')),
    );
  }
  else {
    // There were no entities. Tell the user.
    $content[] = array(
      '#type' => 'item',
      '#markup' => t('No geq_anw entities currently exist.'),
    );
  }
  return $content;
}

/**
 * Callback for a page title when this entity is displayed.
 */
function geq_anw_title($entity) {
  return t('Entity Example Basic (item_description=@item_description)', array('@item_description' => $entity->item_description));
}

/**
 * Menu callback to display an entity.
 *
 * As we load the entity for display, we're responsible for invoking a number
 * of hooks in their proper order.
 *
 * @see hook_entity_prepare_view()
 * @see hook_entity_view()
 * @see hook_entity_view_alter()
 */
function geq_anw_view($entity, $view_mode = 'tweaky') {
  // Our entity type, for convenience.
  $entity_type = 'geq_anw';
  // Start setting up the content.
  $entity->content = array(
    '#view_mode' => $view_mode,
  );
  // Build fields content - this is where the Field API really comes in to play.
  // The task has very little code here because it all gets taken care of by
  // field module.
  // field_attach_prepare_view() lets the fields load any data they need
  // before viewing.
  field_attach_prepare_view($entity_type, array($entity->basic_id => $entity),
    $view_mode);
  // We call entity_prepare_view() so it can invoke hook_entity_prepare_view()
  // for us.
  entity_prepare_view($entity_type, array($entity->basic_id => $entity));
  // Now field_attach_view() generates the content for the fields.
  $entity->content += field_attach_view($entity_type, $entity, $view_mode);

  // OK, Field API done, now we can set up some of our own data.
  $entity->content['created'] = array(
    '#type' => 'item',
    '#title' => t('Created date'),
    '#markup' => format_date($entity->created),
  );
  $entity->content['item_description'] = array(
    '#type' => 'item',
    '#title' => t('Item Description'),
    '#markup' => $entity->item_description,
  );

  // Now to invoke some hooks. We need the language code for
  // hook_entity_view(), so let's get that.
  global $language ;
  echo $langcode = $language->language ;
  // And now invoke hook_entity_view().
  module_invoke_all('entity_view', $entity, $entity_type, $view_mode,$langcode);
  // Now invoke hook_entity_view_alter().
  drupal_alter(array('geq_anw_view', 'entity_view'),$entity->content, $entity_type);

  // And finally return the content.
  return $entity->content;
}

/**
 * Implements hook_field_extra_fields().
 *
 * This exposes the "extra fields" (usually properties that can be configured
 * as if they were fields) of the entity as pseudo-fields
 * so that they get handled by the Entity and Field core functionality.
 * Node titles get treated in a similar manner.
 */
function geq_field_extra_fields() {
  $form_elements['item_description'] = array(
    'label' => t('Item Description'),
    'description' => t('Item Description (an extra form field)'),
    'weight' => -5,
  );
  $display_elements['created'] = array(
    'label' => t('Creation date'),
    'description' => t('Creation date (an extra display field)'),
    'weight' => 0,
  );
  $display_elements['item_description'] = array(
    'label' => t('Item Description'),
    'description' => t('Just like title, but trying to point out that it is a separate property'),
    'weight' => 0,
  );

  // Since we have only one bundle type, we'll just provide the extra_fields
  // for it here.
  $extra_fields['geq_anw']['first_example_bundle']['form'] = $form_elements;
  $extra_fields['geq_anw']['first_example_bundle']['display'] = $display_elements;

  return $extra_fields;
}

/**
 * Provides a wrapper on the edit form to add a new entity.
 */
function geq_anw_add() {
  // Create a basic entity structure to be used and passed to the validation
  // and submission functions.
  $entity = entity_get_controller('geq_anw')->create();
  return drupal_get_form('geq_anw_form', $entity);
}

/**
 * Form function to create an geq_anw entity.
 *
 * The pattern is:
 * - Set up the form for the data that is specific to your
 *   entity: the columns of your base table.
 * - Call on the Field API to pull in the form elements
 *   for fields attached to the entity.
 */
function geq_anw_form($form, &$form_state, $entity) {
 
  $form['item_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Item Description'),
    '#default_value' => $entity->item_description,
  );

  $form['basic_entity'] = array(
    '#type' => 'value',
    '#value' => $entity,
  );

  field_attach_form('geq_anw', $entity, $form, $form_state);

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );
  $form['delete'] = array(
    '#type' => 'submit',
    '#value' => t('Delete'),
    '#submit' => array('geq_anw_edit_delete'),
    '#weight' => 200,
  );

  return $form;
}


/**
 * Validation handler for geq_anw_add_form form.
 * We pass things straight through to the Field API to handle validation
 * of the attached fields.
 */
function geq_anw_form_validate($form, &$form_state) {
  field_attach_form_validate('geq_anw', $form_state['values']['basic_entity'], $form, $form_state);
}


/**
 * Form submit handler: submits basic_add_form information
 */
function geq_anw_form_submit($form, &$form_state) {
  $entity = $form_state['values']['basic_entity'];
  $entity->item_description = $form_state['values']['item_description'];
  field_attach_submit('geq_anw', $entity, $form, $form_state);
  $entity = geq_anw_save($entity);
  $form_state['redirect'] = 'examples/geq/basic/' . $entity->basic_id;
}

/**
 * Form deletion handler.
 *
 * @todo: 'Are you sure?' message.
 */
function geq_anw_edit_delete( $form , &$form_state ) {
  $entity = $form_state['values']['basic_entity'];
  geq_anw_delete($entity);
  drupal_set_message(t('The entity %item_description (ID %id) has been deleted',
    array('%item_description' => $entity->item_description, '%id' => $entity->basic_id))
  );
  $form_state['redirect'] = 'examples/geq';
}

/**
 * We save the entity by calling the controller.
 */
function geq_anw_save(&$entity) {
  return entity_get_controller('geq_anw')->save($entity);
}

/**
 * Use the controller to delete the entity.
 */
function geq_anw_delete($entity) {
  entity_get_controller('geq_anw')->delete($entity);
}

/**
 * EntityExampleBasicControllerInterface definition.
 *
 * We create an interface here because anyone could come along and
 * use hook_entity_info_alter() to change our controller class.
 * We want to let them know what methods our class needs in order
 * to function with the rest of the module, so here's a handy list.
 *
 * @see hook_entity_info_alter()
 */

interface GeqAnwControllerInterface
  extends DrupalEntityControllerInterface {
  public function create();
  public function save($entity);
  public function delete($entity);
}

/**
 * EntityExampleBasicController extends DrupalDefaultEntityController.
 *
 * Our subclass of DrupalDefaultEntityController lets us add a few
 * important create, update, and delete methods.
 */
class GeqAnwController
  extends DrupalDefaultEntityController
  implements GeqAnwControllerInterface {

  /**
   * Create and return a new geq_anw entity.
   */
  public function create() {
    $entity = new stdClass();
    $entity->type = 'geq_anw';
    $entity->basic_id = 0;
    $entity->bundle_type = 'geq_anw';
    $entity->uid = 0;
    $entity->nid = 0;
    $entity->tid = 0;
    $entity->subject_id = 0;
    $entity->score = 0;
    return $entity;
  }

  /**
   * Saves the custom fields using drupal_write_record()
   */
  public function save($entity) {
    // If our entity has no basic_id, then we need to give it a
    // time of creation.
    if (empty($entity->basic_id)) {
      $entity->created = time();
    }
    // Invoke hook_entity_presave().
    module_invoke_all('entity_presave', $entity, 'geq_anw');
    // The 'primary_keys' argument determines whether this will be an insert
    // or an update. So if the entity already has an ID, we'll specify
    // basic_id as the key.
    $primary_keys = $entity->basic_id ? 'basic_id' : array();
    // Write out the entity record.
    drupal_write_record('geq_anw', $entity, $primary_keys);
    // We're going to invoke either hook_entity_update() or
    // hook_entity_insert(), depending on whether or not this is a
    // new entity. We'll just store the name of hook_entity_insert()
    // and change it if we need to.
    $invocation = 'entity_insert';
    // Now we need to either insert or update the fields which are
    // attached to this entity. We use the same primary_keys logic
    // to determine whether to update or insert, and which hook we
    // need to invoke.
    if (empty($primary_keys)) {
      field_attach_insert('geq_anw', $entity);
    }
    else {
      field_attach_update('geq_anw', $entity);
      $invocation = 'entity_update';
    }
    // Invoke either hook_entity_update() or hook_entity_insert().
    module_invoke_all($invocation, $entity, 'geq_anw');
    return $entity;
  }

  /**
   * Delete a single entity.
   *
   * Really a convenience function for delete_multiple().
   */
  public function delete($entity) {
    $this->delete_multiple(array($entity));
  }

  /**
   * Delete one or more geq_anw entities.
   *
   * Deletion is unfortunately not supported in the base
   * DrupalDefaultEntityController class.
   *
   * @param $basic_ids
   *   An array of entity IDs or a single numeric ID.
   */
  public function delete_multiple($entities) {
    $basic_ids = array();
    if (!empty($entities)) {
      $transaction = db_transaction();
      try {
        foreach ($entities as $entity) {
          // Invoke hook_entity_delete().
          module_invoke_all('entity_delete', $entity, 'geq_anw');
          field_attach_delete('geq_anw', $entity);
          $basic_ids[] = $entity->basic_id;
        }
        db_delete('geq_anw')
          ->condition('basic_id', $basic_ids, 'IN')
          ->execute();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('geq', $e);
        throw $e;
      }
    }
  }
}


/**
 * Basic information for the page.
 */
function geq_info_page() {
  $content['preface'] = array(
    '#type' => 'item',
    '#markup' => t('The GEQ Ans Of Geq questions related to the users.')
  );
  if (user_access('administer geq_anw entities')) {
    $content['preface']['#markup'] =  t('You can administer these and add fields and change the view !link.',
      array('!link' => l(t('here'), 'admin/structure/geq_anw/manage'))
    );
  }
  $content['table'] = geq_anw_list_entities();

  return $content;
}

