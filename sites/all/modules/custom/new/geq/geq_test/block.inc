<?php

/**
 * Implements hook_block_info().
 *
 * This hook declares what blocks are provided by the module.
 */
function geq_block_info() {
  $blocks['jquery_counddown'] = array(
    // info: The name of the block.
    'info' => t('Jquery Time'),
    'cache' => DRUPAL_CACHE_PER_ROLE, // default
  );
 return $blocks; 
}


/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function geq_block_view($delta = '') {
    switch ($delta) {
    case 'jquery_counddown':
      $block['subject'] = t('Time:');
      $block['content'] = get_countdown();
      break;
  }
  return $block;
}  


function get_countdown(){
    if(empty($_SESSION['countdown_time'])){
            $_SESSION['countdown_time']=time();
            $max_time_left=variable_get('geq_total_time_for_test',1800);
    }else{
            $st_time =$_SESSION['countdown_time'];
            $cur_time =time();
            $remeaning_min=($cur_time-$st_time);
            $max_time_left=variable_get('geq_total_time_for_test',1800)-$remeaning_min;
    }
     drupal_add_css(PATH_TO_MODULE.'/css/jquery.countdown.css');
     drupal_add_js(PATH_TO_MODULE.'/js/jquery-1.7.1.min.js');
     drupal_add_js(PATH_TO_MODULE.'/js/cound_down/jquery.countdown.js');
     drupal_add_js(
                    array('geq' => array(
                                            'max_time_left' => $max_time_left,
                                            'frm_id' =>'geq-start-test-form'
                                        )
                        ),
                    'setting'
            );    
    drupal_add_js(PATH_TO_MODULE.'/js/geq.js',array('scope'=>'footer'));
    return array('#markup'=>t('<div class="countdown_page">Time Left:</div> <span id="newPage" class="countdown"></span><div class="clear"></div>'));
}
?>
