<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>jQuery UI Dialog Box with Animation</title>
	<link rel="stylesheet" href="css/style.css" type="text/css" media="screen" />
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js" type="text/javascript"></script>
	<script type="text/javascript" src="js/popup.js"></script>
</head>

<body>
<div id="wrap">
<h2><center><button id="displaypopup" style="font-size:14px;padding:9px;margin-bottom:7px;">click for dialog box!</button></center></h2>

<p>Built around CSS3 styles for added effect...</p>

<p>And works in all jQuery-supported browsers!</p>

	<div id="myPopup"></div>

	<div id="backgroundPopup"></div>
</div>
</body>

