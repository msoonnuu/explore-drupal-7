<?php

/**
 * Implements hook_block_info().
 *
 * This hook declares what blocks are provided by the module.
 */
function geq_block_info() {
  $blocks['jquery_counddown'] = array(
    // info: The name of the block.
    'info' => t('Jquery Time'),
    'cache' => DRUPAL_CACHE_PER_ROLE, // default
  );
 
 $blocks['quiz_generator'] = array(
    // info: The name of the block.
    'info' => t('Quiz Generator'),
    'cache' => DRUPAL_CACHE_PER_ROLE, // default
  ); 
  
 return $blocks; 
}


/**
 * Implements hook_block_view().
 *
 * This hook generates the contents of the blocks themselves.
 */
function geq_block_view($delta = '') {
    switch ($delta) {
    case 'jquery_counddown':
      $block['subject'] = t('Time:');
      $block['content'] = get_countdown();
      break;
   
    case 'quiz_generator':
      $block['subject'] = t('Quiz Generator:');
      $block['content'] = drupal_get_form('quiz_gen');
      break;
  
  
  }
  return $block;
}  


function get_countdown(){
    if(empty($_SESSION['countdown_time'])){
            $_SESSION['countdown_time']=time();
            $max_time_left=variable_get('geq_total_time_for_test',1800);
    }else{
            $st_time =$_SESSION['countdown_time'];
            $cur_time =time();
            $remeaning_min=($cur_time-$st_time);
            $max_time_left=variable_get('geq_total_time_for_test',1800)-$remeaning_min;
    }
     drupal_add_css(PATH_TO_MODULE.'/css/jquery.countdown.css');
     drupal_add_js(PATH_TO_MODULE.'/js/jquery-1.7.1.min.js');
     drupal_add_js(PATH_TO_MODULE.'/js/cound_down/jquery.countdown.js');
     drupal_add_js(
                    array('geq' => array(
                                            'max_time_left' => $max_time_left,
                                            'frm_id' =>'geq-start-test-form'
                                        )
                        ),
                    'setting'
            );    
    drupal_add_js(PATH_TO_MODULE.'/js/geq.js',array('scope'=>'footer'));
    return array('#markup'=>t('<div class="countdown_page">Time Left:</div> <span id="newPage" class="countdown"></span><div class="clear"></div>'));
}




/*
 select multiple category and generator test
 */

function quiz_gen($form, &$form_state){
    // Get the list of options to populate the first dropdown.
    $voc = taxonomy_vocabulary_machine_name_load('questions');
    $vid = $voc->vid;
    $parents_term_arr=taxonomy_get_tree($vid, $parent = 0, 1, $load_entities = FALSE);
    $taxonomy_arr = array(""=>"Select Subject");
    foreach($parents_term_arr as $val){
        $taxonomy_arr[$val->tid] = $val->name;
    }

  // If we have a value for the first dropdown from $form_state['values'] we use
  // this both as the default value for the first dropdown and also as a
  // parameter to pass to the function that retrieves the options for the
  // second dropdown.
  $selected = isset($form_state['values']['subject_id']) ? $form_state['values']['subject_id'] : key($taxonomy_arr);
  
  $form['subject_id'] = array(
    '#type' => 'select',
    '#title' => 'Select Subject',
    '#options' => $taxonomy_arr,
    '#default_value' => $selected,
    // Bind an ajax callback to the change event (which is the default for the
    // select form type) of the first dropdown. It will replace the second
    // dropdown when rebuilt
    '#ajax' => array(
      // When 'event' occurs, Drupal will perform an ajax request in the
      // background. Usually the default value is sufficient (eg. change for
      // select elements), but valid values include any jQuery event,
      // most notably 'mousedown', 'blur', and 'submit'.
      // 'event' => 'change',
      'callback' => 'ajax_get_sub_category_callback',
      'wrapper' => 'dropdown-second-replace',
      'method' => 'replace',
      'effect' => 'slide'
    )
  );

  
  $str_js = '
                jQuery(document).ready(function () { 
                            jQuery("#edit-subject-id").change(function() { 
                                jQuery("#dropdown-second-replace").css("display","none"); 
                            });
                             jQuery("#lets_play").click(function() { 
                                 var subject_val = jQuery("#edit-subject-id :selected").val();
                                 if(subject_val=="")
                                 {
                                    alert("Select Subject");
                                    return false;
                                 }
                                 var checked = jQuery("#quiz-gen input:checked").length > 0;
                                if (!checked){
                                    alert("Please check at least one checkbox");
                                    return false;
                                }
                                 var subject = jQuery("#edit-subject-id :selected").text();
                                 var action  = jQuery("#quiz-gen").attr("action");
                                 jQuery("#quiz-gen").attr("action",action+subject+"/auto");
                                 jQuery("#quiz-gen").submit();
                            });
                });     
';
  
  
   $form['subject_id']['#attached']['js'] = array($str_js => array( 'type' => 'inline'));
   
   $form['tids'] = array(
              '#type' => 'checkboxes',
             '#options' => get_terms_opt($selected),
             '#title' => t('Select Topics?'),
             '#prefix' => '<div id="dropdown-second-replace" style="display:none;">',
             '#suffix' => '</div>',  
            );      
   $form['#action'] = url('geq/start_test/');
   $form['submit'] = array(
    '#type' => 'button',
    '#value' => t('Lets Play'),
    '#id'=>'lets_play'   
  );
  return $form;
  
  
}

function ajax_get_sub_category_callback($form, $form_state) {
    return $form['tids'];
}

function get_terms_opt($tid){
     $arr = array();
    $term_ids=taxonomy_get_children($tid);
    foreach($term_ids as $key=>$val){
               $arr_ids[$val->tid] = t($val->name);
           }
   return $arr_ids;         
}





?>
