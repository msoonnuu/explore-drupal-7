/***************************/
//@Author: Adrian "yEnS" Mato Gondelle
//@website: www.yensdesign.com
//@email: yensamg@gmail.com
//@license: Feel free to use it, but keep this credits please!					
/***************************/


var popupStatus = 0;

function loadPopup(){
	if(popupStatus==0){
		$("#backgroundPopup").css({
			"opacity": "0.7"
		});
		//$("#backgroundPopup").fadeIn("slow");
		//$("#myPopup").fadeIn("slow");
		$("#backgroundPopup").slideDown("fast");
		$("#myPopup").slideDown("fast");
		//$("#myPopup").show("scale");
		//$("#backgroundPopup").show("scale");
		popupStatus = 1;
	}
}

function disablePopup(){
	if(popupStatus==1){
		//$("#backgroundPopup").fadeOut("slow");
		//$("#myPopup").fadeOut("slow");
		$("#backgroundPopup").slideUp("fast");
		$("#myPopup").slideUp("fast");
		$("#myPopup").hide("scale");
		$("#backgroundPopup").hide("scale");
		popupStatus = 0;
	}
}

//centering popup
function centerPopup(){
	//request data for centering
	var windowWidth = document.documentElement.clientWidth;
	var windowHeight = document.documentElement.clientHeight;
	var popupHeight = $("#myPopup").height();
	var popupWidth = $("#myPopup").width();
	

	$("#myPopup").css({
		"position": "absolute",
		"top": windowHeight/2-popupHeight/2,
		"left": windowWidth/2-popupWidth/2
	});
	
	$("#backgroundPopup").css({
		"height": windowHeight
	});
}


$(document).ready(function(){
	
	$("#displaypopup").click(function(){
		$('#myPopup').load('http://localhost/popup/get_popup.php');
		//centering with css
		centerPopup();
		//load popup
		loadPopup();
	});
				
	//CLOSING POPUP
	//Click the x event!
	$("#popupClose").click(function(){
		disablePopup();
	});
	//Click out event!
	$("#backgroundPopup").click(function(){
		disablePopup();
	});
	//Press Escape event!
	$(document).keypress(function(e){
		if(e.keyCode==27 && popupStatus==1){
			disablePopup();
		}
	});

});
