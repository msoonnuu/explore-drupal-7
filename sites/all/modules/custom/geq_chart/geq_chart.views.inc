<?php

/*
 Register custom table with view and association with geq entity type
 */
/*
/**
 * Alter the query before executing the query.
 *
 * This hook should be placed in MODULENAME.views.inc and it will be
 * auto-loaded. MODULENAME.views.inc must be in the directory specified by the
 * 'path' key returned by MODULENAME_views_api(), or the same directory as the
 * .module file, if 'path' is unspecified. 
 *
 * @param $view
 *   The view object about to be processed.
 * @param $query
 *   An object describing the query.
 * @see hook_views_query_substitutions()
 */
function geq_chart_views_query_alter(&$view, &$query) {
  // (Example assuming a view with an exposed filter on node title.)
  // If the input for the title filter is a positive integer, filter against
  // node ID instead of node title.
    
  
  
  /*
   Altering array to connect with the subcategory 
   */
  
 /* $query->fields['taxonomy_term_data_geq_anw_1_name'] = array(
                                                                "field"=>"name",
                                                                "table"=>"taxonomy_term_data_geq_anw_1",
                                                                "alias"=>"taxonomy_term_data_geq_anw_1_name"
   
  *                                                             );
  */
 // echo "<pre>";  
 // print_r($query);
//  exit;
  
    /*
     [taxonomy_term_data_geq_anw_name] => Array
                                            (
                                            [field] => name
                                            [table] => taxonomy_term_data_geq_anw
                                            [alias] => taxonomy_term_data_geq_anw_name
                                            )
     */
    
    
    
  if ($view->name == 'my_view' && is_numeric($view->exposed_raw_input['title']) && $view->exposed_raw_input['title'] > 0) {
    // Traverse through the 'where' part of the query.
    foreach ($query->where as &$condition_group) {
      foreach ($condition_group['conditions'] as &$condition) {
        // If this is the part of the query filtering on title, chang the
        // condition to filter on node ID.
        if ($condition['field'] == 'node.title') {
          $condition = array(
            'field' => 'node.nid',
            'value' => $view->exposed_raw_input['title'],
            'operator' => '=',
          );
        }
      }
    }
  }
}


function geq_chart_views_default_views_alter(&$views){
  $handler =& $view->display['DISPLAY_ID']->handler;
  //print_r($handler);exit;
  switch($views->name){
      case 'maths_topper':
        pr($views);
        exit;  
      break;    
  } 
  
}


/**
 * This hook is called right before the render process. The query has been
 * executed, and the pre_render() phase has already happened for handlers, so
 * all data should be available.
 *
 * Adding output to the view can be accomplished by placing text on
 * $view->attachment_before and $view->attachment_after. Altering the content
 * can be achieved by editing the items of $view->result.
 *
 * This hook can be utilized by themes.
 * @param $view
 *   The view object about to be processed.
 */
function geq_chart_views_pre_render(&$view) {
      $results_view = array('results');  
      
      //echo "<pre>";
      
    //  print_r($view);
      
    //  exit;
      
      
      //$results = &$view->result;
      foreach ($view->result as $key => $result) {
        if ($view->name == 'results') {
           // $view->result[$key]->geq_anw_score = "yes";
             // if($view->result[$key]->geq_anw_score==1){
             //  $view->result[$key]->geq_anw_score = "yes"; 
            //  }
            //  else{
             //   $view->result[$key]->geq_anw_score = "no";   
            //  }
        }    
      } 
       
      /*
      foreach ($results as $key => $result) {
        if ($view->name == 'results') {
               echo $results[$key]->geq_anw_score; 
        }    
      }
      */
  //shuffle($view->result);

}