<?php
 // note_service.inc
    /**
     * Callback for creating note resources.
     *
     * @param object $data
     * @return object
     */
    function _note_service_create($data) {
      global $user;

      unset($data->id);
      $data->uid = $user->uid;
      $data->created = time();
      $data->modified = time();

      if (!isset($data->subject)) {
        return services_error('Missing note attribute subject', 406);
      }

      if (!isset($data->note)) {
        return services_error('Missing note attribute note', 406);
      }

      note_service_write_note($data);
      return (object)array(
        'id' => $data->id,
        'uri' => services_resource_uri(array('note', $data->id)),
      );
    }
    
    
    
     // note_service.inc
    /**
     * Callback for updating note resources.
     *
     * @param int $id
     * @param object $data
     * @return object
     */
    function _note_service_update($id, $data) {
      global $user;
      $note = note_service_get_note($id);

      unset($data->created);
      $data->id = $id;
      $data->uid = $note->uid;
      $data->modified = time();

      note_service_write_note($data);
      return (object)array(
        'id' => $id,
        'uri' => services_resource_uri(array('note', $id)),
      );
    }
    
    
     // note_service.inc
    /**
     * Callback for retrieving note resources.
     *
     * @param int $id
     * @return object
     */
    function _note_service_retrieve($id) {
      return note_service_get_note($id);
    }

    /**
     * Callback for deleting note resources.
     *
     * @param int $id
     * @return object
     */
    function _note_service_delete($id) {
      note_service_delete_note($id);
      return (object)array(
        'id' => $id,
      );
    }
    
    
     // note_service.inc
    /**
     * Callback for listing notes.
     *
     * @param int $page
     * @param array $parameters
     * @return array
     */
    function _note_service_index($page, $parameters) {
      global $user;

      $notes = array();
      $res = db_query("SELECT * FROM {note} WHERE uid=:uid ORDER BY modified DESC", array(
        ':uid' => $user->uid,
      ));

      while ($note = db_fetch_object($res)) {
        $notes[] = $note;
      }

      return $notes;
    }
    
    
    
    
    
    
?>

