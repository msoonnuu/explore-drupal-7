<?PHP

/**
 *
 * @param type $type 
 * @return form  
 */
// $all_question_status all--> questions belong to  all childrens  same->questions belonf to same category  

function geq_start_test($type,$all_question_status){
    return drupal_get_form('geq_start_test_form', $type,$all_question_status);
}

function get_radio_elements($question_info,$offset,$length,$form_state){
   $arr = array();
   $slice_arr=array_slice($question_info, $offset,$length,true);
   $sno = $offset+1;
   
   $current_step = $form_state['step'];
   $current_step--;
   if(!empty($form_state['values'])){
   $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];
   }
   
   foreach($slice_arr as $key=>$val)
   {
       $opt_arr = array(
                            t($val->field_opt_one['und'][0]['value']) => t($val->field_opt_one['und'][0]['value']),
                            t($val->field_opt_two['und'][0]['value']) => t($val->field_opt_two['und'][0]['value']),
                            t($val->field_opt_three['und'][0]['value']) => t($val->field_opt_three['und'][0]['value']),
                            t($val->field_op_four['und'][0]['value']) => t($val->field_op_four['und'][0]['value'])
                       );
       $arr['questions_'.$val->nid] = array(
        '#type' => 'radios',
        '#title' => t($sno.". ".$val->title),
        '#default_value'=>!empty($form_state['step_information'][$current_step]['stored_values']['questions_'.$val->nid])?$form_state['step_information'][$current_step]['stored_values']['questions_'.$val->nid]:'',   
        '#options' => $opt_arr
      );
       $sno++;
   }
   return $arr;
}

function geq_start_test_form($form,&$form_state,$type,$all_question_status){
 global $user;
  
 $form['#attributes'] = array('id'=>'geq-start-test-form','name'=>'myform');
 $form['uid'] =  array('#type'=>"hidden","#value"=>$user->uid);
 $form['#prefix'] = '<div id="pp_login"></div><div id="wizard-form-wrapper">';
 $form['#suffix'] = "</div>";
 if (empty($form_state['step'])) {
    $form_state['step'] = 1;
  }
  $step = &$form_state['step'];
  drupal_set_title(t('Start Test:'));
  //drupal_set_title(t('Start Test: Step @step', array('@step' => $step)));
  $form = geq_get_questions_per_steps($form, $form_state,$type,$all_question_status,$step);
  if ($step > 1) {
    $form['prev'] = array(
      '#type' => 'button',
      '#value' => t('Previous'),
      '#name' => 'prev',
       '#ajax' => array(
        'wrapper' => 'wizard-form-wrapper',
        'callback' => 'form_example_wizard_previous_submit',
        'method' => 'replace',
        'effect' => 'fade'   
      )
    ); 
  }
  $step_arr=get_step_wise_index_arr();
  if ($step < count($step_arr['step_index_info'])) {
     $form['next'] = array(
      '#type' => 'button',
      '#value' => t('Next'),
      '#name' => 'next',
      '#ajax' => array(
        'wrapper' => 'wizard-form-wrapper',
        'callback' => 'form_example_wizard_next_submit',
        'method' => 'replace',
        'effect' => 'fade',
      ),
     '#prefix'=>"<div id='pp'></div>",
     
    ); 
  }
  
   if(!user_is_logged_in()){
   $form['finish'] = array(
      '#type' => 'button',
      '#value' => t('Finish'),
      '#id'=>'geq_finish',
      '#ajax' => array(
        'wrapper' => 'pp_login',
        'callback' => 'geq_modal_forms_login',
        'method' => 'replace',
        'effect' => 'fade',
        'path'=>'/geq/js/login'  
      ),
     '#attributes'=>array('class'=>array('ctools-modal-ctools-sample-style'))  
    ); 
   }
   else{
           $form['finish'] = array(
          '#type' => 'submit',
          '#value' => t('Finish'),
          '#id'=>'geq_finish',
        );
   }
  return $form;
}

function geq_get_questions_per_steps($form,&$form_state,$type,$all_question_status,$step){
  if(empty($form_state['questions'])){
      $questions_info_arr = get_questions_by(array('type'=>$type,'all_question_status'=>$all_question_status));
      $question_info = $questions_info_arr['questions'];
      $form_state['questions']=$questions_info_arr['questions'];
  }
  else{
      $question_info = $form_state['questions'];
  }
  $step_arr=get_step_wise_index_arr();
  $form+=get_radio_elements($question_info,$step_arr['step_index_info'][$step-1]['start_index'],$step_arr['step_index_info'][$step-1]['length'],$form_state);
  return $form;
}



/**
 * Submit handler for the "previous" button.
 * - Stores away $form_state['values']
 * - Decrements the step counter
 * - Replaces $form_state['values'] with the values from the previous state.
 * - Forces form rebuild.
 *
 * You are not required to change this function.
 *
 * @ingroup form_example
 */
function form_example_wizard_previous_submit($form, &$form_state) {
  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];
  if ($current_step > 1) {
    $current_step--;
    $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
  }
 // $form_state['rebuild'] = TRUE;
  $form_state['step'] = $current_step;
  return drupal_rebuild_form('geq_start_test_form',$form_state);
}

/**
 * Submit handler for the 'next' button.
 * - Saves away $form_state['values']
 * - Increments the step count.
 * - Replace $form_state['values'] from the last time we were at this page
 *   or with array() if we haven't been here before.
 * - Force form rebuild.
 *
 * You are not required to change this function.
 *
 * @param $form
 * @param $form_state
 *
 * @ingroup form_example
 */
function form_example_wizard_next_submit($form, &$form_state) {
  $current_step = &$form_state['step'];
  $step_arr=get_step_wise_index_arr();
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];
  if ($current_step < count($step_arr['step_index_info'])) {
    $current_step++;
    if (!empty($form_state['step_information'][$current_step]['stored_values'])) {
      $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
    }
    else {
      $form_state['values'] = array();
    }
    $form_state['rebuild'] = TRUE;  // Force rebuild with next step.
    $form_state['step'] = $current_step;
    return drupal_rebuild_form('geq_start_test_form',$form_state);
  }
}

/**
 * Returns form elements for the 'other info' page of the wizard. This is the
 * thid and last step of the example wizard.
 *
 * @ingroup form_example
 */

function geq_start_test_form_validate($form, &$form_state){
}

/**
 * Wizard form submit handler.
 * - Saves away $form_state['values']
 * - Process all the form values.
 *
 * This demonstration handler just do a drupal_set_message() with the information
 * collected on each different step of the wizard.
 *
 * @param $form
 * @param $form_state
 *
 * @ingroup form_example
 */
function geq_start_test_form_submit($form, &$form_state) {
          global $user;  
          $form_state['rebuild'] = TRUE;
          $current_step = &$form_state['step'];
          $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];
          foreach($form_state['questions'] as $key=>$val){
             $questions_arr[$val->nid] =0;  
           }
           foreach ($form_state['step_information'] as $index => $value) {
               foreach($value['stored_values'] as $key=>$val){
                       if(strstr($key,"questions_")){
                                  $nid  = end(explode("_",$key));
                                 $question_info = entity_load('node',array($nid));
                                 $question_info  = $question_info[$nid];
                                 if(trim($question_info->field_anwer['und'][0]['value'])==$val){
                                   $questions_arr[$nid]=array('score'=>1,'your_answer'=>$val);  
                                 }else{
                                     $questions_arr[$nid]=array('score'=>0,'your_answer'=>$val);;  
                                 }
                        }
               }  
           } 
            geq_save_test_data($questions_arr);
}

/** 
 *This function manage steps in  the form
 */

function get_step_wise_index_arr(){
   $output_step_info  = array();
   $output_step_info['tot_no_of_step']=round(TOT_QUE/TOT_QUE_PER_PAGE);
   for($i=1;$i<=$output_step_info['tot_no_of_step'];$i++){
     
      $start_index = ($i-1)*TOT_QUE_PER_PAGE;
     
      $length   =TOT_QUE_PER_PAGE;
     if(!$length){
         $length=1;
     }
     if((TOT_QUE/TOT_QUE_PER_PAGE)%2!=0 && $i==$output_step_info['tot_no_of_step']){
       $length=$length+1;
     }
     $output_step_info["step_index_info"][] = array("start_index"=>$start_index,"length"=>$length);
   }
   return $output_step_info;  
}


/* 
this function will take term id and return parent  
 */

function get_parent_term_id($t_id){
    $parr = taxonomy_get_parents($t_id);
    $subject_id="";
    foreach($parr as $pval){
        $subject_id = $pval->tid;
    }
    return $subject_id;
}

function geq_save_test_data($questions_arr)
{
        global $user;
        $geq_master_obj = new stdClass();
        $geq_master_obj->id='';
        $geq_master_obj->uid=$user->uid;
        $geq_master_obj->created=time();
        drupal_write_record('geq_master', $geq_master_obj);
        $test_id =$geq_master_obj->id;
        foreach($questions_arr as $key=>$val){
             $question_info = entity_load('node',array($key));
             $all_parents=taxonomy_get_parents($question_info->field_type_geq['und'][0]['tid']);
             $question_info  = $question_info[$key];  
             $geq_anw_obj=entity_get_controller('geq_anw')->create();
             $geq_anw_obj->basic_id='';
             $geq_anw_obj->test_id=$test_id;
             $geq_anw_obj->uid=$user->uid;
             $geq_anw_obj->subject_id=get_parent_term_id($question_info->field_type_geq['und'][0]['tid']);
             $geq_anw_obj->score=$val['score'];  
             $geq_anw_obj->nid=$key;
             $geq_anw_obj->your_answer=$val['your_answer'];
             $geq_anw_obj->bundle_type='geq';
             $geq_anw_obj->tid=$question_info->field_type_geq['und'][0]['tid'];
             $geq_anw_obj->created=time();
             entity_get_controller('geq_anw')->save($geq_anw_obj);
        }
        
        $message = t("Your test has been completed successfully, Please review your result.");
        drupal_set_message($message,'status');  
        drupal_goto('user/test_analysis/'.$test_id);
        exit;
}

